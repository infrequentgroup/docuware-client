import Methods from './methods.js'

/**
 * Basics endpoints
 * @class
 */
export default class Basics{

    constructor(token, orgID){
        this.token = token;
        this.orgID = orgID;
    }
    
    /**
     * Get a list of an organizations accounting years.
     * @returns {Promise<Object>}
     */
     getOrganization(){
        return new Promise((res, rej) => {
            Methods.getOrganization(this.token, this.orgID).then((response) => {
                return res(response.data);
            }).catch(err => rej(this.processError(err)));
        })
    }

    processError(err){
        console.log(err);
    }
}