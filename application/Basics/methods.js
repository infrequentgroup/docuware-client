import DineroRequest from '../../libs/DineroRequest.js';

export default {
    getOrganization(token, orgID){
        let req = new DineroRequest(token, orgID,"https://api.dinero.dk/v1/{organizationId}/accountingyears");
        return req.executeGet();
    }
}