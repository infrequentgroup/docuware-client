import axios from 'axios';

export default class DineroRequest {
    constructor(token, orgId, url){
        this.token = token;
        this.orgId = orgId;
        this.hosturl = this.replaceOrg(url);
    }
    replaceOrg(url){
        return url.replace('{organizationId}',this.orgId);
    }
    replaceField(name,data){
        this.hosturl = this.hosturl.replace(name,data);
    }
    executeGet(){
        return axios.default({
            url: this.hosturl,
            method: "GET",
            followRedirects: true,
            maxRedirects: 5,
            headers: this.getHeaders()
        });
    }
    executePost(data){
        return axios.default({
            url: this.hosturl,
            method: "POST",
            followRedirects: true,
            maxRedirects: 5,
            headers: this.getHeaders(),
            data:data
        });
    }
    executeDelete(data) {
        return axios.default({
            url: this.hosturl,
            method: "DELETE",
            followRedirects: true,
            maxRedirects: 5,
            headers: this.getHeaders(),
            data: data
        })
    }
    executePut(data) {
        return axios.default({
            url: this.hosturl,
            method: "PUT",
            followRedirects: true,
            maxRedirects: 5,
            headers: this.getHeaders(),
            data: data
        }).catch(err => console.log(err))
    }
    executePatch(data) {
        return axios.default({
            url: this.hosturl,
            method: "PATCH",
            followRedirects: true,
            maxRedirects: 5,
            headers: this.getHeaders(),
            data: data
        })
    }
    getHeaders() {
        return {
            'Authorization': 'Bearer ' + this.token,
            'Content-Type': 'application/json'
        }
    }
}